import { Colegio } from "./colegio";

export class Curso {
    idCurso:number;
    grado:string;
    salon:string;
    colegio:Colegio;

    constructor(){
        this.idCurso=0;
        this.grado='';
        this.salon='';
        this.colegio=new Colegio;
    }
}
