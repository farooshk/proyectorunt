import { Component, OnInit } from '@angular/core';
import { Asignatura } from './asignatura';
import { AsignaturaService } from './asignatura.service';

@Component({
  selector: 'app-asignaturas',
  templateUrl: './asignaturas.component.html',
  styleUrls: ['./asignaturas.component.css'],
  providers: [AsignaturaService]
})
export class AsignaturasComponent implements OnInit {

  asignaturas:Asignatura[] = [];

  constructor(private asignaturaService: AsignaturaService) {
  }

  ngOnInit(): void {
    this.asignaturaService.getAll().subscribe(
      data => {this.asignaturas=data;
        console.log(data);
      }
    );
  }

}
