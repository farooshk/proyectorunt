import { Curso } from "./curso";
import { Estudiante } from "./estudiante";
import { Profesor } from "./profesor";

export class Asignatura {
    idAsignatura:number;
    nombre:string;
    profesor:Profesor;
    estudiantes:Array<Estudiante>;
    curso:Curso;

    constructor(){
        this.idAsignatura=0;
        this.nombre='';
        this.profesor= new Profesor;
        this.estudiantes= [];
        this.curso= new Curso;

    }

}
