import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AsignaturasComponent } from './asignaturas/asignaturas.component';
import { Asignaturas2Component } from './asignaturas2/asignaturas2.component';
import { Asignaturas3Component } from './asignaturas3/asignaturas3.component';

const routes: Routes = [
  {path: 'profesor1', component: AsignaturasComponent},
  {path: 'profesor2', component: Asignaturas2Component},
  {path: 'profesor3', component: Asignaturas3Component},
  {path: '**', component: AsignaturasComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
