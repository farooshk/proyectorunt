import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AsignaturasComponent } from './asignaturas/asignaturas.component';
import { Asignaturas2Component } from './asignaturas2/asignaturas2.component';
import { Asignaturas3Component } from './asignaturas3/asignaturas3.component';

@NgModule({
  declarations: [
    AppComponent,
    AsignaturasComponent,
    Asignaturas2Component,
    Asignaturas3Component,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
