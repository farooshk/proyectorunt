import { Component, OnInit } from '@angular/core';
import { Asignatura } from '../asignaturas/asignatura';
import { AsignaturaService } from '../asignaturas/asignatura.service';

@Component({
  selector: 'app-asignaturas2',
  templateUrl: './asignaturas2.component.html',
  styleUrls: ['./asignaturas2.component.css'],
  providers: [AsignaturaService]
})
export class Asignaturas2Component implements OnInit {

  asignaturas:Asignatura[] = [];

  constructor(private asignaturaService: AsignaturaService) {
  }

  ngOnInit(): void {
    this.asignaturaService.getAll().subscribe(
      data => {this.asignaturas=data;
        console.log(data);
      }
    );
  }

}
