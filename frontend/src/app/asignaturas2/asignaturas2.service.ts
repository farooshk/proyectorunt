import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Asignatura } from '../asignaturas/asignatura';

@Injectable({
  providedIn: 'root'
})
export class AsignaturaService {

  private url:string="/api/getasignaturalist";

  constructor(private http: HttpClient) { 
    console.log("Servicio conectado!")
   }

  getAll():Observable<Asignatura[]> {
    return this.http.get<Asignatura[]>(this.url);
  }
}
