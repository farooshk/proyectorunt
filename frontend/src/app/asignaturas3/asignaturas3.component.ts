import { Component, OnInit } from '@angular/core';
import { Asignatura } from '../asignaturas/asignatura';
import { AsignaturaService } from '../asignaturas/asignatura.service';

@Component({
  selector: 'app-asignaturas3',
  templateUrl: './asignaturas3.component.html',
  styleUrls: ['./asignaturas3.component.css'],
  providers: [AsignaturaService]
})
export class Asignaturas3Component implements OnInit {

  asignaturas:Asignatura[] = [];

  constructor(private asignaturaService: AsignaturaService) {
  }

  ngOnInit(): void {
    this.asignaturaService.getAll().subscribe(
      data => {this.asignaturas=data;
        console.log(data);
      }
    );
  }

}
