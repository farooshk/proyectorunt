package com.proyectRunt.crud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proyectRunt.crud.model.Estudiante;
import com.proyectRunt.crud.service.CrudServiceEstudiante;

@RestController
public class CrudRestControllerEstudiante {
	@Autowired
	private CrudServiceEstudiante service;
	@CrossOrigin(origins = "http://localhost:8888")
	@GetMapping("/api/getestudiantelist")
	public List<Estudiante> fetchEstudianteList(){
		List<Estudiante> estudiante = new ArrayList<Estudiante>();
		estudiante = service.fetchestudianteList();
		return estudiante;
	
	}
}
