package com.proyectRunt.crud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proyectRunt.crud.model.Asignatura;
import com.proyectRunt.crud.service.CrudServiceAsignatura;

@RestController
public class CrudRestControllerAsignatura {
	@Autowired
	private CrudServiceAsignatura service;
	@CrossOrigin(origins = "http://localhost:8888")
	@GetMapping("/api/getasignaturalist")
	public List<Asignatura> fetchAsignaturaList(){
		List<Asignatura> asignatura = new ArrayList<Asignatura>();
		asignatura = service.fetchasignaturaList();
		return asignatura;
	
	}
}
