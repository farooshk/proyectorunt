package com.proyectRunt.crud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proyectRunt.crud.model.Colegio;
import com.proyectRunt.crud.service.CrudServiceColegio;

@RestController
public class CrudRestControllerColegio {
	@Autowired
	private CrudServiceColegio service;
	@CrossOrigin(origins = "http://localhost:8888")
	@GetMapping("/api/getcolegiolist")
	public List<Colegio> fetchColegioList(){
		List<Colegio> colegio = new ArrayList<Colegio>();
		colegio = service.fetchcolegioList();
		return colegio;
	
	}
}
