package com.proyectRunt.crud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proyectRunt.crud.model.Profesor;
import com.proyectRunt.crud.service.CrudServiceProfesor;

@RestController
public class CrudRestControllerProfesor {
	@Autowired
	private CrudServiceProfesor service;
	@CrossOrigin(origins = "http://localhost:8888")
	@GetMapping("/api/getprofesorlist")
	public List<Profesor> fetchProfesorList(){
		List<Profesor> profesor = new ArrayList<Profesor>();
		profesor = service.fetchprofesorList();
		return profesor;
	
	}
}
