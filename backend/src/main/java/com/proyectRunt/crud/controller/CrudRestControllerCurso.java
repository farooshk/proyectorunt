package com.proyectRunt.crud.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.proyectRunt.crud.model.Curso;
import com.proyectRunt.crud.service.CrudServiceCurso;

@RestController
public class CrudRestControllerCurso {
	@Autowired
	private CrudServiceCurso service;
	@CrossOrigin(origins = "http://localhost:8888")
	@GetMapping("/api/getcursolist")
	public List<Curso> fetchCursoList(){
		List<Curso> curso = new ArrayList<Curso>();
		curso = service.fetchcursoList();
		return curso;
	
	}
}
