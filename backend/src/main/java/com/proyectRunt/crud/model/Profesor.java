package com.proyectRunt.crud.model;

import java.util.List;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name="PROFESOR")
public class Profesor {
    
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idProfesor;
	@Column
    private String nombre;
	@OneToMany
    private List<Asignatura> asignaturas;

    public Profesor() {
    }

    public Profesor(int idProfesor, String nombre) {
        this.nombre = nombre;
    }

    public Profesor(List<Asignatura> asignaturas) {
        this.asignaturas = asignaturas;
    }
    
    public int getIdProfesor() {
        return idProfesor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setIdProfesor(int idProfesor) {
        this.idProfesor = idProfesor;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setAsignatura(Asignatura asignatura){
        asignaturas.add(asignatura);
    }

	@Override
	public int hashCode() {
		return Objects.hash(asignaturas, idProfesor, nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Profesor other = (Profesor) obj;
		return Objects.equals(asignaturas, other.asignaturas) && idProfesor == other.idProfesor
				&& Objects.equals(nombre, other.nombre);
	}

	@Override
	public String toString() {
		return "Profesor [idProfesor=" + idProfesor + ", nombre=" + nombre + ", asignaturas=" + asignaturas + "]";
	}

}
