package com.proyectRunt.crud.model;

import java.util.List;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name="CURSO")
public class Curso {
    
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idCurso;
	@Column
    private String grado;
	@Column
    private String salon;
	@ManyToMany
	private List<Asignatura> asignaturas;
	@ManyToOne
    private Colegio colegio;

    public Curso() {
    }

    public Curso(int idCurso, String grado, String salon, Colegio colegio) {
        this.grado = grado;
        this.salon = salon;
        this.colegio = colegio;
    }

    public Curso(List<Asignatura> asignaturas) {
        this.asignaturas = asignaturas;
    }
    
    public int getIdCurso() {
        return idCurso;
    }

    public String getGrado() {
        return grado;
    }

    public void setIdCurso(int idCurso) {
        this.idCurso = idCurso;
    }

    public void setGrado(String grado) {
        this.grado = grado;
    }

    public String getSalon() {
        return salon;
    }

    public void setSalon(String salon) {
        this.salon = salon;
    }

    public Colegio getColegio() {
        return colegio;
    }

    public void setColegio(Colegio colegio) {
        this.colegio = colegio;
    }
    
    public void setAsignatura(Asignatura asignatura){
        asignaturas.add(asignatura);
    }

    @Override
	public int hashCode() {
		return Objects.hash(asignaturas, colegio, grado, idCurso, salon);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Curso other = (Curso) obj;
		return Objects.equals(asignaturas, other.asignaturas) && Objects.equals(colegio, other.colegio)
				&& Objects.equals(grado, other.grado) && idCurso == other.idCurso && Objects.equals(salon, other.salon);
	}

	@Override
	public String toString() {
		return "Curso [idCurso=" + idCurso + ", grado=" + grado + ", salon=" + salon + ", asignaturas=" + asignaturas
				+ ", colegio=" + colegio + "]";
	}

}
