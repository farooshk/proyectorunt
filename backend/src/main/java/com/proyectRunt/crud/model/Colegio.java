package com.proyectRunt.crud.model;

import java.util.List;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name="COLEGIO")
public class Colegio {
    
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idColegio;
	@Column
    private String nombre;
	@OneToMany
	private List<Curso> cursos;
    
    public Colegio() {
    }

    public Colegio(int idColegio, String nombre) {
        this.idColegio = idColegio;
        this.nombre = nombre;
    }
    
    public Colegio(List<Curso> cursos) {
        this.cursos = cursos;
    }

    public int getIdColegio() {
    	return idColegio;
    }
    
    public String getNombre() {
        return nombre;
    }

    public void setIdColegio(int idColegio) {
    	this.idColegio = idColegio;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setCursos(Curso curso){
        cursos.add(curso);
    }

	@Override
	public int hashCode() {
		return Objects.hash(cursos, idColegio, nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Colegio other = (Colegio) obj;
		return Objects.equals(cursos, other.cursos) && idColegio == other.idColegio
				&& Objects.equals(nombre, other.nombre);
	}

	@Override
	public String toString() {
		return "Colegio [idColegio=" + idColegio + ", nombre=" + nombre + ", cursos=" + cursos + "]";
	}
    
}
