package com.proyectRunt.crud.model;

import java.util.List;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name="ESTUDIANTE")
public class Estudiante {
    
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idEstudiante;
	@Column
    private String nombre;
	@ManyToMany
    private List<Asignatura> asignaturas;

    public Estudiante() {
    }

    public Estudiante(int idEstudiante, String nombre) {
        this.nombre = nombre;
    }

    public Estudiante(List<Asignatura> asignaturas) {
        this.asignaturas = asignaturas;
    }
    
    public int getIdEstudiante() {
        return idEstudiante;
    }

    public String getNombre() {
        return nombre;
    }

    public void setIdEstudiante(int idEstudiante) {
        this.idEstudiante = idEstudiante;
    }
    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public void setAsignatura(Asignatura asignatura){
        asignaturas.add(asignatura);
    }

	@Override
	public int hashCode() {
		return Objects.hash(asignaturas, idEstudiante, nombre);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estudiante other = (Estudiante) obj;
		return Objects.equals(asignaturas, other.asignaturas) && idEstudiante == other.idEstudiante
				&& Objects.equals(nombre, other.nombre);
	}

	@Override
	public String toString() {
		return "Estudiante [idEstudiante=" + idEstudiante + ", nombre=" + nombre + ", asignaturas=" + asignaturas + "]";
	}

}
