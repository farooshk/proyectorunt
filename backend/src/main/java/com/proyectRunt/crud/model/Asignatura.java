package com.proyectRunt.crud.model;

import java.util.List;
import java.util.Objects;

import javax.persistence.*;

@Entity
@Table(name="ASIGNATURA")
public class Asignatura {
    
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idAsignatura;
	@Column
    private String nombre;
	@ManyToOne
    private Profesor profesor;
	@ManyToMany
	private List<Estudiante> estudiantes;
    @ManyToOne
    private Curso curso;

    public Asignatura() {
    }

    public Asignatura(int idAsignatura, String nombre, Profesor profesor, Curso curso, List<Estudiante> estudiantes) {
        this.nombre = nombre;
        this.profesor = profesor;
        this.curso = curso;
        this.estudiantes = estudiantes;
    }

    public Asignatura(List<Estudiante> estudiantes) {
        this.estudiantes = estudiantes;
    }
    
    public int getIdAsignatura() {
        return idAsignatura;
    }

    public String getNombre() {
        return nombre;
    }

    public Profesor getProfesor() {
        return profesor;
    }

    public Curso getCurso() {
        return curso;
    }

    public List<Estudiante> getEstudiantes() {
		return estudiantes;
	}

	public void setIdAsignatura(int idAsignatura) {
        this.idAsignatura = idAsignatura;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    public void setCurso(Curso curso) {
        this.curso = curso;
    }
    
    public void setEstudiante(Estudiante estudiante){
        estudiantes.add(estudiante);
    }

	public void setEstudiantes(List<Estudiante> estudiantes) {
		this.estudiantes = estudiantes;
	}

	@Override
	public int hashCode() {
		return Objects.hash(curso, estudiantes, idAsignatura, nombre, profesor);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Asignatura other = (Asignatura) obj;
		return Objects.equals(curso, other.curso) && Objects.equals(estudiantes, other.estudiantes)
				&& idAsignatura == other.idAsignatura && Objects.equals(nombre, other.nombre)
				&& Objects.equals(profesor, other.profesor);
	}

}
