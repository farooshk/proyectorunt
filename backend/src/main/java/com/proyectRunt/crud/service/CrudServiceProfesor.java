package com.proyectRunt.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyectRunt.crud.model.Profesor;
import com.proyectRunt.crud.repository.CrudRepoProfesor;

@Service
public class CrudServiceProfesor {
	@Autowired
	private CrudRepoProfesor repo;
	
	public List<Profesor> fetchprofesorList(){
		return repo.findAll();
	}
}
