package com.proyectRunt.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyectRunt.crud.model.Colegio;
import com.proyectRunt.crud.repository.CrudRepoColegio;

@Service
public class CrudServiceColegio {
	@Autowired
	private CrudRepoColegio repo;
	
	public List<Colegio> fetchcolegioList(){
		return repo.findAll();
	}
}
