package com.proyectRunt.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyectRunt.crud.model.Asignatura;
import com.proyectRunt.crud.repository.CrudRepoAsignatura;

@Service
public class CrudServiceAsignatura {
	@Autowired
	private CrudRepoAsignatura repo;
	
	public List<Asignatura> fetchasignaturaList(){
		return repo.findAll();
	}
}
