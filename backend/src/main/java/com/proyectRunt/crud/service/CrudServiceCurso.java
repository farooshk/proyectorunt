package com.proyectRunt.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyectRunt.crud.model.Curso;
import com.proyectRunt.crud.repository.CrudRepoCurso;

@Service
public class CrudServiceCurso {
	@Autowired
	private CrudRepoCurso repo;
	
	public List<Curso> fetchcursoList(){
		return repo.findAll();
	}
}
