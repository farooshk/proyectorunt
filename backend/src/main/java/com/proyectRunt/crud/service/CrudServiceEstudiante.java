package com.proyectRunt.crud.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.proyectRunt.crud.model.Estudiante;
import com.proyectRunt.crud.repository.CrudRepoEstudiante;

@Service
public class CrudServiceEstudiante {
	@Autowired
	private CrudRepoEstudiante repo;
	
	public List<Estudiante> fetchestudianteList(){
		return repo.findAll();
	}
}
