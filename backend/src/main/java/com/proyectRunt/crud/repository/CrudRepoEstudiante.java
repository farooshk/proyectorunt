package com.proyectRunt.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyectRunt.crud.model.Estudiante;

public interface CrudRepoEstudiante extends JpaRepository<Estudiante, Integer> {

}
