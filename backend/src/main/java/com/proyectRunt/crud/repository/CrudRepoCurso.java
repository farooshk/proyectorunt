package com.proyectRunt.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyectRunt.crud.model.Curso;

public interface CrudRepoCurso extends JpaRepository<Curso, Integer> {

}
