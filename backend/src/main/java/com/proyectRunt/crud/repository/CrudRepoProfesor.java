package com.proyectRunt.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyectRunt.crud.model.Profesor;

public interface CrudRepoProfesor extends JpaRepository<Profesor, Integer> {

}
