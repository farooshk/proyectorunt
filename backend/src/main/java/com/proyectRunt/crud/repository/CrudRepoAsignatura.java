package com.proyectRunt.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyectRunt.crud.model.Asignatura;

public interface CrudRepoAsignatura extends JpaRepository<Asignatura, Integer> {

}
