package com.proyectRunt.crud.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.proyectRunt.crud.model.Colegio;

public interface CrudRepoColegio extends JpaRepository<Colegio, Integer> {

}
